<?php

namespace MediaWiki\Extension\PageReForms\Tests\Integration\JSONScript;

use SMW\Tests\JSONScriptServicesTestCaseRunner;
use MediaWiki\Extension\PageReForms\Hook;

/**
 * @group PageReForms
 * @group medium
 *
 * @license GNU GPL v3+
 *
 * @author hexmode
 */
class JSONScriptTestCaseRunnerTest extends JSONScriptServicesTestCaseRunner {

	/**
	 * @see JSONScriptServicesTestCaseRunner::runTestAssertionForType
	 */
	protected function runTestAssertionForType( string $type ) : bool {

		$expectedAssertionTypes = [
			'parser',
			'parser-html',
			'special',
			'api'
		];

		return in_array( $type, $expectedAssertionTypes );
	}

	/**
	 * @see JSONScriptTestCaseRunner::getTestCaseLocation
	 */
	protected function getTestCaseLocation() {
		return __DIR__ . '/TestCase';
	}

	/**
	 * @see JSONScriptTestCaseRunner::getTestCaseLocation
	 */
	protected function getRequiredJsonTestCaseMinVersion() {
		return '2';
	}

	/**
	 * @see JSONScriptTestCaseRunner::getDependencyDefinitions
	 */
	protected function getDependencyDefinitions() {
		return [
			'PageReForms' => function( $version, &$reason ) {
				return class_exists( Hook::class );
			}
		];
	}
}
