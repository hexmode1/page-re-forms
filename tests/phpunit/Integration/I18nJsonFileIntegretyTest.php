<?php

namespace MediaWiki\Extension\PageReForms\Tests\PHPUnit\Integration;

use DirectoryIterator;
use JsonException;
use PHPUnit\Framework\Testcase;

/**
 * @group PageReForms
 *
 * @license GNU GPL v3+
 *
 * @author MarkAHershberger
 */
class I18nJsonFileIntegrityTest extends TestCase {

	/**
	 * @dataProvider i18nFileProvider
	 */
	public function testI18NJsonDecodeEncode( $entry ) {

		$name = basename( $entry, ".json" );
		$this->assertTrue( is_readable( $entry ), "$name: file is readable" );

		$str = file_get_contents( $entry );
		$this->assertIsString( $str, "$name: contents are read" );

		$extract = null;
		try {
			$extract = json_decode( $str, true, 512, JSON_THROW_ON_ERROR );
		} catch ( JsonException $e ) {
			$this->assertNotInstanceOf( JsonException::class, $e, "$name: json parsed" );
		}

		$this->assertIsArray( $extract, "$name: assoc array" );
	}

	public function i18nFileProvider() {

		$provider = [];
		$location = dirname( dirname( dirname( __DIR__ ) ) ) . "/i18n";

		$iterator = new DirectoryIterator( $location );

		foreach ( $iterator as $entry ) {
			if ( $entry->isFile() && $entry->getExtension() === "json" ) {
				$provider[ $entry->getBaseName() ] = [ $entry->getPathname() ];
			}
		}

		asort( $provider );
		return $provider;
	}

}
