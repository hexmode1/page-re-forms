<?php
/**
 * @file
 * @ingroup PF
 */

/**
 * @ingroup PFFormInput
 */
class PFEndDateInput extends PFDateInput {

	public static function getName(): string {
		return 'end date';
	}

	public static function getDefaultCargoTypes(): array {
		return [
			'End date' => [],
		];
	}

	public static function getDefaultPropTypes(): array {
		return [];
	}

	public static function getOtherPropTypesHandled(): array {
		return [ '_dat' ];
	}

	public function getInputClass(): string {
		return 'dateInput endDateInput';
	}
}
