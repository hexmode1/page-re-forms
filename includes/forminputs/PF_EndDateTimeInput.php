<?php
/**
 * @file
 * @ingroup PF
 */

/**
 * @ingroup PFFormInput
 */
class PFEndDateTimeInput extends PFDateTimeInput {

	public static function getName(): string {
		return 'end datetime';
	}

	public function getInputClass(): string {
		return 'dateTimeInput endDateTimeInput';
	}

	public static function getDefaultCargoTypes(): array {
		return [
			'End datetime' => [],
		];
	}
}
